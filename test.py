import cv2
import os
import argparse
import glob
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable
from models_form_GAN import *
from utils import *
from skimage.measure import compare_ssim as ssim

# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"

parser = argparse.ArgumentParser(description="FormNet_Test")
parser.add_argument("--base_layers", type=int, default=17, help="Number of base layers")
parser.add_argument("--form_layers", type=int, default=3, help="Number of form layers")
parser.add_argument("--modeldir", type=str, default="models/form_netGANsig15.pth", help='path of log files')
#parser.add_argument("--modeldir", type=str, default="models/form_netGANsig25.pth", help='path of log files')
#parser.add_argument("--modeldir", type=str, default="models/form_netGANsig45.pth", help='path of log files')
#parser.add_argument("--modeldir", type=str, default="models/form_netGANsig75.pth", help='path of log files')
parser.add_argument("--test_data", type=str, default='Set14', help='test on Set14 or Kodak or BSD')
parser.add_argument("--test_noiseL", type=float, default=15, help='noise level used on test set')
opt = parser.parse_args()

def normalize(data):
    return data/255.

def main():
    # Build model
    print('Loading model ...\n')
    net = FormNet(channels=1, base_layers=opt.base_layers, form_layers=opt.form_layers)
    device_ids = [0]
    model = nn.DataParallel(net, device_ids=device_ids).cuda()
    model.load_state_dict(torch.load(opt.modeldir))
    model.eval()
    # load data info
    print('Loading data info ...\n')
    if opt.test_data!='BSD':
        files_source = glob.glob(os.path.join('data', opt.test_data, '*.png'))
    else:
        files_source = glob.glob(os.path.join('data', opt.test_data, '*.jpg'))
    files_source.sort()
    # process data
    psnr_test = 0
    sm_test=0
    torch.manual_seed(0)
    for f in files_source:
        # image
        Img = cv2.imread(f)
        Img = normalize(np.float32(Img[:,:,0]))
        Img = np.expand_dims(Img, 0)
        Img = np.expand_dims(Img, 1)
        ISource = torch.Tensor(Img)
        # noise
        noise = torch.FloatTensor(ISource.size()).normal_(mean=0, std=opt.test_noiseL/255.)
        # noisy image
        INoisy = ISource + noise
        INoisy=torch.clamp(INoisy,0.,1.)
        ISource, INoisy = Variable(ISource.cuda()), Variable(INoisy.cuda())
        Out = torch.clamp(model(INoisy), 0., 1.)

        #cv2.imwrite('data/'+opt.test_data+'_out/'+f[-6:],Out.cpu().data.numpy().squeeze()*255)

        psnr = batch_PSNR(Out, ISource, 1.)
        psnr_test += psnr
        Out_sm=Out.cpu().data.numpy().squeeze()
        ISource_sm=ISource.cpu().data.numpy().squeeze()
        sm=ssim(Out_sm, ISource_sm,data_range=1.)
        sm_test += sm
        print("%s PSNR %f, SSIM %f" % (f, psnr,sm))

    psnr_test /= len(files_source)
    sm_test /= len(files_source)
    print("\n(PSNR,SSIM) on test data (%f, %f)" % (psnr_test,sm_test))

if __name__ == "__main__":
    main()
