# FormNet: Formatted Learning for Image Restoration

By [Jianbo Jiao](https://jianbojiao.com/), [Wei-Chih Tu](https://sites.google.com/site/wctu1009/), [Ding Liu](https://scholar.google.com/citations?user=PGtHUI0AAAAJ&hl=en), [Shengfeng He](http://www.shengfenghe.com/), [Rynson W. H. Lau](http://www.cs.cityu.edu.hk/~rynson/), [Thomas S. Huang](http://ifp-uiuc.github.io/).


### Introduction

This repository contains the codes and models described in the paper "[FormNet: Formatted Learning for Image Restoration](https://ieeexplore.ieee.org/document/9084384)". FormNet is an end-to-end network for image restoration that takes advantage of formatted learning.

### Usage

0. This project was implemented with the [PyTorch](https://pytorch.org) framework with [TorchVision](https://pytorch.org/docs/0.2.0/) on Linux with GPUs. Please setup the environment according to the [instructions](https://pytorch.org/get-started/previous-versions/) first.
1. The used data is stored under folder "[data](data/)" and the models are stored in folder "[models](models/)".
2. To test the performance of the model, please run file "[test.py](test.py)".
3. To train the model from scratch, please use the code "[train.py](train.py)".

### Citation

If you find these codes and models useful, please cite:

	@InProceedings{Jianbo_2020TIP,
		author = {Jianbo Jiao, Wei-Chih Tu, Ding Liu, Shengfeng He, Rynson W. H. Lau, Thomas S. Huang},
		title = {FormNet: Formatted Learning for Image Restoration},
		booktitle = {IEEE Transactions on Image Processing},
		month = {April},
		year = {2020}
	}
	
**References**

[1] He K, Zhang X, Ren S, Sun J. [Deep residual learning for image recognition](http://openaccess.thecvf.com/content_cvpr_2016/papers/He_Deep_Residual_Learning_CVPR_2016_paper.pdf). InProceedings of the IEEE conference on computer vision and pattern recognition 2016.

[2] [Kodak Lossless True Color Image Suite](http://r0k.us/graphics/kodak/index.html).

[3] Code borrows from this [repo](https://github.com/SaoYan/DnCNN-PyTorch).

You may also find the project for our previous version here: [FormResNet](https://bitbucket.org/JianboJiao/formresnet/src/master/)

If you have any questions please email the [authors](mailto:jiaojianbo.i@gmail.com or jianbo@robots.ox.ac.uk)