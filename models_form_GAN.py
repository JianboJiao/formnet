import torch
import torch.nn as nn

class FormNet(nn.Module):
    def __init__(self, channels, base_layers=10, form_layers=10):
        super(FormNet, self).__init__()
        kernel_size = 3
        padding = 1
        features = 64
        layers = []
        layers.append(nn.Conv2d(in_channels=channels, out_channels=features, kernel_size=kernel_size, padding=padding, bias=False))
        layers.append(nn.ReLU(inplace=True))

        for _ in range(base_layers-2):
            layers.append(nn.Conv2d(in_channels=features, out_channels=features, kernel_size=kernel_size, padding=padding, bias=False))
            layers.append(nn.BatchNorm2d(features))
            layers.append(nn.ReLU(inplace=True))
        layers.append(nn.Conv2d(in_channels=features, out_channels=channels, kernel_size=kernel_size, padding=padding, bias=False))
        self.basenet = nn.Sequential(*layers)
        layers_form = []
        layers_form.append(nn.Conv2d(in_channels=channels, out_channels=features, kernel_size=kernel_size, padding=padding, bias=False))
        layers_form.append(nn.ReLU(inplace=True))
        for _ in range(form_layers-2):
            layers_form.append(nn.Conv2d(in_channels=features, out_channels=features, kernel_size=kernel_size, padding=padding, bias=False))
            layers_form.append(nn.BatchNorm2d(features))
            layers_form.append(nn.ReLU(inplace=True))
        layers_form.append(nn.Conv2d(in_channels=features, out_channels=channels, kernel_size=kernel_size, padding=padding, bias=False))
        self.formnet = nn.Sequential(*layers_form)

    def forward(self, x):
        base_out = self.basenet(x)
        base_out = base_out+x
        out = self.formnet(base_out)
        final_out = base_out+out

        return final_out

class netD2(nn.Module):
    def __init__(self,opt,channels=1):
        super(netD2, self).__init__()
        opt.ndf=64
        opt.out=3
        self.main = nn.Sequential(
            nn.Conv2d(channels,opt.ndf,4,2,1,bias=False),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf,opt.ndf*2,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ndf*2),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf*2,opt.ndf*4,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ndf*4),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf*4,opt.ndf*8,4,2,1,bias=False),
            nn.BatchNorm2d(opt.ndf*8),
            nn.LeakyReLU(0.2,inplace=True),
            nn.Conv2d(opt.ndf*8, 1,opt.out,1,0,bias=False),
            nn.Sigmoid()
        )

    def forward(self, input):
        ouput=self.main(input)

        return ouput.view(-1,1)
