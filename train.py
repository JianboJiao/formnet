import os
import argparse
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.utils as utils
from torch.autograd import Variable
from torch.utils.data import DataLoader
from models_form_GAN import *
from dataset import prepare_data, Dataset
from utils import *
from vgg import Vgg16

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

parser = argparse.ArgumentParser(description="FormNet")
parser.add_argument("--preprocess", type=bool, default=False, help='run prepare_data or not')#set to True for the first time
parser.add_argument("--batchSize", type=int, default=112, help="Training batch size")#128
parser.add_argument("--base_layers", type=int, default=17, help="Number of base layers") #17
parser.add_argument("--form_layers", type=int, default=3, help="Number of form layers") #3
parser.add_argument("--epochs", type=int, default=50, help="Number of training epochs")
parser.add_argument("--milestone", type=int, default=30, help="When to decay learning rate; should be less than epochs")
parser.add_argument("--lr", type=float, default=1e-3, help="Initial learning rate") #1e-3
parser.add_argument("--outf", type=str, default="models", help='path of log files')
parser.add_argument("--mode", type=str, default="S", help='with known noise level (S) or blind training (B)')
parser.add_argument("--noiseL", type=float, default=45, help='noise level; ignored when mode=B')
parser.add_argument("--val_noiseL", type=float, default=45, help='noise level used on validation set')
parser.add_argument("--base_path",type=str,default='')
parser.add_argument("--save_path", type=str,default="form_netGANsig45_t.pth")
opt = parser.parse_args()

def main():
    # Load dataset
    print('Loading dataset ...\n')
    dataset_train = Dataset(train=True)
    dataset_val = Dataset(train=False)
    loader_train = DataLoader(dataset=dataset_train, num_workers=4, batch_size=opt.batchSize, shuffle=True)
    print("# of training samples: %d\n" % int(len(dataset_train)))

    label=torch.FloatTensor(opt.batchSize)
    real_label=1
    fake_label=0
    label=Variable(label)

    # Build model
    net = FormNet(channels=1, base_layers=opt.base_layers, form_layers=opt.form_layers)
    net.apply(weights_init_kaiming)

    netD=netD2(opt)
    netD.apply(weights_init_kaiming)

    criterion = nn.MSELoss(size_average=False)
    criterionD=nn.BCELoss()
    # Sobel loss--
    a=np.array([[1,0,-1],[2,0,-2],[1,0,-1]])
    SobelX=nn.Conv2d(1,1,3,1,1,bias=False)
    SobelX.weight=nn.Parameter(torch.from_numpy(a).float().unsqueeze(0).unsqueeze(0))
    b=np.array([[1,2,1],[0,0,0],[-1,-2,-1]])
    SobelY=nn.Conv2d(1,1,3,1,1,bias=False)
    SobelY.weight=nn.Parameter(torch.from_numpy(b).float().unsqueeze(0).unsqueeze(0))
    def critL1(input,target):
        return torch.sum(torch.abs(input-target))/input.data.nelement()
    #------------
    # Feat loss--
    vgg=Vgg16(requires_grad=False)
    def critL2(input,target):
        return torch.sum((input-target)**2)/input.data.nelement()
    #------------
    if opt.base_path:
        print("load base net from %s"%opt.base_path)
        base_net=torch.load(opt.base_path,map_location=lambda storage, loc:storage)
        model_dict=net.state_dict()
        from collections import OrderedDict
        new_state_dict=OrderedDict()
        for k,v in base_net.items():
            name=k[7:]
            if name in model_dict:
                new_state_dict[name]=v
        model_dict.update(new_state_dict)
        net.load_state_dict(model_dict)
    # Move to GPU
    device_ids = [0]
    model = nn.DataParallel(net, device_ids=device_ids).cuda()
    netD.cuda()
    criterion.cuda()
    criterionD.cuda()
    label=label.cuda()
    SobelX.cuda()
    SobelY.cuda()
    vgg.cuda()
    # Optimizer
    if not opt.base_path:
        optimizer = optim.Adam(model.parameters(), lr=opt.lr)
    else:
        optimizer = optim.Adam([
            {'params': [param for name,param in model.named_parameters()\
                    if name.startswith('module.basenet')], 'lr':1e-2*opt.lr}
        ], lr=opt.lr)
    optimizerD=optim.Adam(netD.parameters(),lr=opt.lr,betas=(0.5,0.999))
    # training
    noiseL_B=[0,55] # ingnored when opt.mode=='S'
    for epoch in range(0,opt.epochs):
        if epoch < opt.milestone:
            current_lr = opt.lr
        else:
            for param_group in optimizer.param_groups:
                current_lr=param_group["lr"]
                param_group["lr"] = current_lr/10
        print('learning rate %f' % current_lr)
        # train
        for i, data in enumerate(loader_train, 0):
            # training step
            model.train()
            optimizer.zero_grad()
            img_train = data
            if opt.mode == 'S':
                noise = torch.FloatTensor(img_train.size()).normal_(mean=0, std=opt.noiseL/255.)
            if opt.mode == 'B':
                noise = torch.zeros(img_train.size())
                stdN = np.random.uniform(noiseL_B[0], noiseL_B[1], size=noise.size()[0])
                for n in range(noise.size()[0]):
                    sizeN = noise[0,:,:,:].size()
                    noise[n,:,:,:] = torch.FloatTensor(sizeN).normal_(mean=0, std=stdN[n]/255.)
            imgn_train = img_train + noise
            img_train, imgn_train = Variable(img_train.cuda()), Variable(imgn_train.cuda())

            #train with real
            netD.zero_grad()
            label.data.resize_(img_train.size(0)).fill_(real_label)
            output=netD(img_train)
            errD_real=criterionD(output,label)
            errD_real.backward()
            # D_x=output.data.mean()
            #train with fake
            out_train = model(imgn_train)
            label.data.fill_(fake_label)
            output_Dn=netD(out_train.detach())
            errD_fake=criterionD(output_Dn,label)
            errD_fake.backward()
            # D_G_z1=output_Dn.data.mean()
            #errD=errD_real+errD_fake
            optimizerD.step()
            #update G net
            model.zero_grad()
            label.data.fill_(real_label)
            output_Dn=netD(out_train)
            errG_D=criterionD(output_Dn,label)

            loss_Sobel=critL1(SobelX(out_train),SobelX(img_train))+\
                       critL1(SobelY(out_train),SobelY(img_train))
            loss_Sobel=loss_Sobel / 2
            out_train3=torch.cat((out_train,out_train,out_train),1)
            img_train3=torch.cat((img_train,img_train,img_train),1)
            loss_Feat=critL2(vgg(out_train3).relu2_2,vgg(img_train3).relu2_2)
            loss_MSE = criterion(out_train, img_train) / (imgn_train.size()[0]*2)
            loss=loss_MSE*0.4+loss_Sobel*0.3+loss_Feat*0.3 + 0.01*errG_D
            loss.backward()
            optimizer.step()

            # results
            if i%100==0:
                model.eval()
                out_train = torch.clamp(model(imgn_train), 0., 1.)
                psnr_train = batch_PSNR(out_train, img_train, 1.)
                print("[epoch %d][%d/%d] loss: %.4f MSE: %.4f Sobel: %.4f Feat: %.4f Loss_GD: %.4f PSNR_train: %.4f" %
                    #(epoch+1, i+1, len(loader_train), loss.data[0], loss_MSE.data[0], loss_Sobel.data[0], loss_Feat.data[0], errG_D.data[0], psnr_train))
                    (epoch+1, i+1, len(loader_train), loss.data.item(), loss_MSE.data.item(), loss_Sobel.data.item(), loss_Feat.data.item(), errG_D.data.item(), psnr_train))

            del loss, loss_MSE, loss_Sobel, loss_Feat, errG_D, errD_real,errD_fake,img_train,imgn_train,out_train3,img_train3,output,output_Dn
        ## the end of each epoch
        model.eval()
        # validate
        psnr_val = 0
        for k in range(len(dataset_val)):
            img_val = torch.unsqueeze(dataset_val[k], 0)
            noise = torch.FloatTensor(img_val.size()).normal_(mean=0, std=opt.val_noiseL/255.)
            imgn_val = img_val + noise
            img_val, imgn_val = Variable(img_val.cuda()), Variable(imgn_val.cuda())
            out_val = torch.clamp(model(imgn_val), 0., 1.)
            psnr_val += batch_PSNR(out_val, img_val, 1.)
            del img_val,imgn_val,out_val
        psnr_val /= len(dataset_val)
        print("\n[epoch %d] PSNR_val: %.4f" % (epoch+1, psnr_val))
        if True:
            # save model
            torch.save(model.state_dict(), os.path.join(opt.outf, opt.save_path))

if __name__ == "__main__":
    if opt.preprocess:
        if opt.mode == 'S':
            prepare_data(data_path='data', patch_size=60, stride=10, aug_times=1)#40
        if opt.mode == 'B':
            prepare_data(data_path='data', patch_size=50, stride=10, aug_times=2)
    main()
